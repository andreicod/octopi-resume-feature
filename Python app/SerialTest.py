import time
import serial

ser = serial.Serial(
	port='/dev/ttyUSB0',
	baudrate=2400,
	parity=serial.PARITY_NONE,
	stopbits=serial.STOPBITS_ONE,
	bytesize=serial.EIGHTBITS
)

ser.close()
ser.open()
ser.isOpen()

ser.flushInput()
ser.flushOutput()

while True:
	data = ser.read()
	ser.flushInput()
	print(data)
	if data :
		print('UPS on battery')
	time.sleep(5)
