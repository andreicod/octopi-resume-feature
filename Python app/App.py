#!/usr/bin/env python3
from octorest import OctoRest
import sys
import time
import serial
from serial import Serial

ser = serial.Serial(
        port='/dev/ttyUSB0',
        baudrate=2400,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS
)

def make_client():
    try:
        client = OctoRest(url="http://octopi.local", apikey="7FFE424287544A3C9DB4138D49227801")
        return client
    except Exception as e:
        print(e)


def get_status(client):
    try:
        printing = client.printer()['state']['flags']['printing']
        if printing:
            message = "Printing"
        else:
            message = "Paused"
        return message

    except Exception as e:
        print(e)


def main():
	ser.close()
	ser.open()
	ser.isOpen()

	ser.flushInput()
	ser.flushOutput()

	client = make_client()

	while True:
		data = ser.read()
		ser.flushInput()
		if data:
			print('UPS on battery')
			if get_status(client) == 'Printing':
				client.pause()
				print('Pausing print')

if __name__ == "__main__":
    main()

