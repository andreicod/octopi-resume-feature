from octorest import OctoRest
import sys


def make_client():
    try:
        client = OctoRest(url="http://octopi.local", apikey="7FFE424287544A3C9DB4138D49227801")
        return client
    except Exception as e:
        print(e)


def get_status(client):
    try:
        printing = client.printer()['state']['flags']['printing']
        if printing:
            message = "Printing"
        else:
            message = "Paused"
        return message

    except Exception as e:
        print(e)


def main():
	client = make_client()
	input = sys.stdin
	for line in input:
		if line.strip() == 'exit':
			print('Terminating program')
			exit(0)
		elif line.strip() == 'pause' and get_status(client) != "Paused":
			client.pause()
			print('Pausing print')
		elif line.strip() == 'resume' and get_status(client) != "Printing":
			client.resume()
			print('Resuming print')
		elif line.strip() == 'cancel':
			client.cancel()
			print('Cancelling print')
		elif line.strip() == 'status':
			print(get_status(client))
		elif line.strip() == 'help':
			print('Available commands:')
			print('exit pause resume cancel help')

if __name__ == "__main__":
    main()
