OctoPi resume feature
This is a feature for octopi that reads from a ups throgh a usb port if it is on battery and pauses the print automatically so the printer consumes very little power.

First you have to download the github repository in /home/pi folder
git clone https://ninjaforest@bitbucket.org/ninjaforest/octopi-resume-feature.git

Cura settings:
On your desktop open cura and create a new printer profile.
Select G-code flavor to Marlin.
Change the Start G-code and the Stop G-code with the scripts under the folder "Cura Gcode Scripts"

Instructions to run feature in octoprint:
1.You have to change in octoprint the "After Print Paused" and "Before Print Paused" gcode scripts. You cand find the scripts under the folder "OctoPi Gcode Scripts".

2.Generate a new app API key from octoprint and copy paste it in the App.py file, in the field "apikey", located in Python app.

3.If you installed octopi through the octopi image you won't have the pip3 package manager and we will need it. Run the following commands in the terminal:
sudo apt-get update
sudo apt install python3-pip
sudo apt-get upgrade
sudo rebootbopss12345


4.Let's install the dependencies that we will need in this project
sudo pip3 install octorest
sudo pip3 install serial

5.Start the script in the background (this should be done everytime you reboot octopi unitl I figure a solution)
cd /home/pi/octopi-resume-feature/Python\ app/
chmod +x App.py
nohup /home/pi/octopi-resume-feature/Python\ app/App.py &
